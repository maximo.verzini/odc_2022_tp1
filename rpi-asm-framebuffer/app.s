.include "functions.s"


.equ SCREEN_WIDTH, 		640
.equ SCREEN_HEIGH, 		480
.equ BITS_PER_PIXEL,  	32



.globl main
main:
	// X0 contiene la direccion base del framebuffer
 	mov x20, x0	// Save framebuffer base address to x20	
	//---------------- CODE HERE ------------------------------------
Fondo:
    mov x5, SCREEN_WIDTH

	mov x10, 0
	mov x2, SCREEN_HEIGH         // Y Size Y = 480
loop1:
	mov x1, SCREEN_WIDTH         // X Size x = 640
loop0:
	stur w10,[x0]	   // Set color of pixel N
	add x0,x0,4	   // Next pixel
	sub x1,x1,1	   // decrement X counter 
	cbnz x1,loop0	   // If not end row jump
	sub x2,x2,1	   // Decrement Y counter
	cbnz x2,loop1	   // if not last row, jump

Letras:
    movz x10, 0x04, lsl 16
	movk x10, 0xC718, lsl 00   
    mov x0, x20  // posicion base    
    mov x1, xzr // indice de cuad          
    mov x3, 3 // eje x
    mov x4, 3 // eje y
    mov x2, 6 // largo linea
    add x2, x2, x4  
    bl LineaVertical
    mov x3, 32 // eje x
    mov x4, 50 // eje y
    mov x6, 6 // largo linea
    mov x9, x6
    bl LineaHorizontal
    
    
    


    mov x21, 11 // repeticion de sprite
    mov x24, 0 // y + x14 para bajar y unidades y repetir
    mov x19, 0
    mov x22, 3 // ESCALAR, cantidad de pixeles por unidad (1 = 3 pixeles juntos)
    mov x23, 2 // repeticion de filas
    

Marciano_numero1:

HeadArgs:
    movz x10, 0x04, lsl 16     // color verde 
	movk x10, 0xC718, lsl 00   
    mov x0, x20  // posicion base    
    mov x1, xzr // indice de cuad          
    mov x3, 16 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 18 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x5, SCREEN_WIDTH //640
    mov x6, 7 //  largo cuad
    mul x6, x6, x22
    mov x9, x6
    mov x2, 4 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4  
    bl Cuadraditos

Eyes:
    mov x10, 0// color rojo             
    mov x3, 17 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 19 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos
           
    mov x3, 21 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 19 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos

antenas:
    movz x10, 0x04, lsl 16     // color verde 
	movk x10, 0xC718, lsl 00 // color            
    mov x3, 17 // eje x  
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 17 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1//  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos
                
    mov x3, 21 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 17 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos
        
    mov x3, 16 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 16 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos
         
    mov x3, 22 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 16 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos

MarcianitoArms:
    mov x3, 15 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 19 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 2 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos

    mov x3, 23 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 19 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 2 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos

    mov x3, 14 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 20 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 3 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos

    mov x3, 24 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 20 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 3 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos
    
Mouth:
    mov x3, 16 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 22 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos

    mov x3, 22 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 22 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos
    
    mov x3, 17 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 23 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 2 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos

    mov x3, 20 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 23 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 2 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos


Fila: 
    add x19, x19, 50
    sub x21, x21, 1
    cbnz x21, Marciano_numero1
    mov x19, 0
    mov x21, 11
    add x24, x24, 50
    sub x23, x23, 1
    cbnz x23, Marciano_numero1 

mov x19, 0
mov x21, 11
mov x24, 50
mov x23, 2


Marciano_numero2:

HeadArgs2:
    movz x10, 0xFF, lsl 16 // color rojo
    mov x3, 18
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 30  
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 9 // longitud de vertice
    mov x9, x6  // x9 = 6
    mov x2, 5 // altura de base hasta vertice
    mul x2, x2, x22
    add x2, x2, x4
    mov x14, 4 
    mov x15, x14
    bl Triangulito
    
    mov x3, 14 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 35 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 11  //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos
    mov x4, 36 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 11  //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos

Eyes2:
    movz x10, 0
    mov x3, 17 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 35 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos

    mov x3, 21 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 35 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos

Tentacles:
    movz x10, 0xFF, lsl 16 
    mov x3, 17 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 37 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos
    mov x3, 16 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 38 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos

    mov x3, 21 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 37 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos

    mov x3, 22 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 38 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos
     
    mov x3, 18 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 38 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 3 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos

    mov x3, 17 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 39 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos
    mov x3, 21 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 39 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos
    mov x3, 15 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 39 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos

    mov x3, 23 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 39 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 1 //  largo cuad
    mul x6, x6, x22
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos

Fila2: 
    add x19, x19, 50
    sub x21, x21, 1
    cbnz x21, Marciano_numero2 
    mov x19, 0
    mov x21, 11
    add x24, x24, 50
    sub x23, x23, 1
    cbnz x23, Marciano_numero2 

mov x19, 0
mov x21, 8
mov x24, 150
mov x23, 2

Marciano_numero3:


HeadArgs3:
    movz x10, 0x19, lsl 16
	movk x10, 0x6BF0, lsl 0
    mov x3, 17
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 31  
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 17 // longitud de vertice
    mov x9, x6
    mov x2, 14 // altura de base hasta vertice
    add x2, x2, x4
    mov x14, 5 
    mov x15, x14
    bl Triangulito2
    mov x3, 35 // eje x
    add x3, x3, x19
    mov x4, 36 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 47 //  largo cuad
    mov x9, x6 
    mov x2, 6 // alto cuadrado
    add x2, x2, x4
    bl Cuadraditos

    mov x3, 14 // eje x
    mul x3, x3, x22
    add x3, x3, x19
    mov x4, 32 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 34  //  largo cuad
    mov x9, x6 
    mov x2, 2 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos
Eyes3:
    movz x10, 0
    mov x3, 46 // eje x
    add x3, x3, x19
    mov x4, 36 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 8 //  largo cuad
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos

    mov x3, 64 // eje x
    add x3, x3, x19
    mov x4, 36 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 8 //  largo cuad
    mov x9, x6 
    mov x2, 1 // alto cuadrado
    mul x2, x2, x22
    add x2, x2, x4
    bl Cuadraditos

Tentacles2:
    movz x10, 0x19, lsl 16
	movk x10, 0x6BF0, lsl 0
    mov x3, 46 // eje x
    add x3, x3, x19
    mov x4, 38 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 8 //  largo cuad
    mov x9, x6 
    mov x2, 4 // alto cuadrado
    add x2, x2, x4
    bl Cuadraditos
    mov x3, 64 // eje x
    add x3, x3, x19
    mov x4, 38 // eje y
    mul x4, x4, x22
    add x4, x4, x24
    mov x6, 8 //  largo cuad
    mov x9, x6 
    mov x2, 4 // alto cuadrado
    add x2, x2, x4
    bl Cuadraditos

    mov x3, 41 // eje x
    add x3, x3, x19
    mov x4, 119 // eje y
    add x4, x4, x24
    mov x6, 8 //  largo cuad
    mov x9, x6 
    mov x2, 3 // alto cuadrado
    add x2, x2, x4
    bl Cuadraditos
    mov x3, 55 // eje x
    add x3, x3, x19
    mov x4, 119 // eje y
    add x4, x4, x24
    mov x6, 8 //  largo cuad
    mov x9, x6 
    mov x2, 3 // alto cuadrado
    add x2, x2, x4
    bl Cuadraditos
    mov x3, 69 // eje x
    add x3, x3, x19
    mov x4, 119 // eje y
    add x4, x4, x24
    mov x6, 8 //  largo cuad
    mov x9, x6 
    mov x2, 3 // alto cuadrado
    add x2, x2, x4
    bl Cuadraditos

    mov x3, 33 // eje x
    add x3, x3, x19
    mov x4, 123 // eje y
    add x4, x4, x24
    mov x6, 8 //  largo cuad
    mov x9, x6 
    mov x2, 3 // alto cuadrado
    add x2, x2, x4
    bl Cuadraditos
    mov x3, 77 // eje x
    add x3, x3, x19
    mov x4, 123 // eje y
    add x4, x4, x24
    mov x6, 8 //  largo cuad
    mov x9, x6 
    mov x2, 3 // alto cuadrado
    add x2, x2, x4
    bl Cuadraditos
Fila3: 
    add x19, x19, 70  // DISTANCIA ENTRE CADA REPETICION DE SPRITE
    sub x21, x21, 1
    cbnz x21, Marciano_numero3 

mov x19, 0
mov x21, 4
mov x24, 150
mov x23, 2




Player: 

    movz x10, 0xFF, lsl 16
	movk x10, 0xFFFF, lsl 00
    mov x3, 250 // eje x
    mov x4, 460 // eje y
    mov x6, 50 // largo
    mov x9, x6 
    mov x2,  11// alto cuadrado
    add x2, x2, x4
    bl Cuadraditos
    mov x3, 255 // eje x
    mov x4, 455 // eje y
    mov x6, 40 // largo
    mov x9, x6 
    mov x2,  10 // alto cuadrado
    add x2, x2, x4
    bl Cuadraditos
    mov x3, 271 // eje x
    mov x4, 448 // eje y
    mov x6, 8 // largo
    mov x9, x6 
    mov x2,  10 // alto cuadrado
    add x2, x2, x4
    bl Cuadraditos
    

mov x19, 0
mov x21, 4
mov x24, 50
mov x23, 2



Coberturas:


    movz x10, 0x07, lsl 16
	movk x10, 0xA111, lsl 00
    mov x3, 70
    add x3, x3, x19
    mov x4, 365  
    mov x6, 30 // longitud de vertice
    mov x9, x6  // x9 = 6
    mov x2, 7 // altura de base hasta vertice
    add x2, x2, x4
    mov x14, 2 
    mov x15, x14
    bl Triangulito
    mov x3, 55 // eje x
    add x3, x3, x19
    mov x4, 373 // eje y
    mov x6, 60 // largo
    mov x9, x6 
    mov x2,  30 // alto cuadrado
    add x2, x2, x4
    bl Cuadraditos
    movz x10, 0, lsl 16 // color rojo
    mov x3, 77
    add x3, x3, x19
    mov x4, 393  
    mov x6, 15 // longitud de vertice
    mov x9, x6  // x9 = 6
    mov x2, 5 // altura de base hasta vertice
    add x2, x2, x4
    mov x14, 2 
    mov x15, x14
    bl Triangulito
    mov x3, 69 // eje x
    add x3, x3, x19
    mov x4, 398 // eje y
    mov x6, 31 // largo
    mov x9, x6 
    mov x2,  15 // alto cuadrado
    add x2, x2, x4
    bl Cuadraditos
FilaBarriers: 
    add x19, x19, 150
    sub x21, x21, 1
    cbnz x21, Coberturas

mov x19, 0
mov x21, 4
mov x24, 50
mov x23, 2

DisparosPlayer:
    
    movz x10, 0xFF, lsl 16
	movk x10, 0xFFFF, lsl 00
    mov x3, 274 // eje x
    mov x4, 400 // eje y
    mov x2, 9 // largo linea
    add x2, x2, x4 
    bl LineaVertical

    

DisparosMarcianos:
    mov x3, 274
    mov x4, 350  
    mov x14, 5
    mov x15, x14
    bl LineaDiagonalDer
    mov x3, 279
    mov x4, 355  
    mov x14, 5
    mov x15, x14
    bl LineaDiagonalIzq
    mov x3, 275
    mov x4, 360  
    mov x14, 5
    mov x15, x14
    bl LineaDiagonalDer




    b exit  // IMPORTANTE si no damos exit se ejecuta el programa de vuelta



    



exit:

	//---------------------------------------------------------------
	// Infinite Loop 

InfLoop: 
	b InfLoop
